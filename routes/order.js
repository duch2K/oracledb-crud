const Router = require('express')

const controller = require('../controllers/order')

const router = new Router()

router.get('/payment-methods/:id?', controller.getPaymentMethods)
router.get('/:id?', controller.getOrders)
router.post('/', controller.createOrder)
router.put('/make-order', controller.makeOrder)
router.put('/:id?', controller.updateOrder)
router.delete('/:id?', controller.deleteOrder)

module.exports = router