const Router = require('express')

const customerRouter = require('./customer')
const orderRouter = require('./order')
const productRouter = require('./product')
const categoryRouter = require('./category')
const authRouter = require('./auth')

const router = new Router()

router.use('/customer', customerRouter)
router.use('/order', orderRouter)
router.use('/product', productRouter)
router.use('/category', categoryRouter)
router.use('/auth', authRouter)

module.exports = router

