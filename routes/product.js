const Router = require('express')

const controller = require('../controllers/product')

const router = new Router()

router.get('/by-category', controller.getProductsByCategories)
router.get('/cart/:id', controller.getOrderItems)
router.get('/:id?', controller.getProducts)
router.post('/', controller.createProduct)
router.post('/add-to-order', controller.createOrderItem)
router.put('/:id?', controller.updateProduct)
router.put('/order-item-inc-dec/:id?', controller.updateOrderItemIncDec)
router.put('/order-item/:id?', controller.updateOrderItem)
router.delete('/:id?', controller.deleteProduct)
router.delete('/order-item/:id?', controller.deleteOrderItem)

module.exports = router