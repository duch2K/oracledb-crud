const Router = require('express')

const controller = require('../controllers/customer')

const router = new Router()

router.get('/:id?', controller.getCustomers)
router.post('/', controller.createCustomer)
router.put('/:id?', controller.updateCustomer)
router.delete('/:id?', controller.deleteCustomer)

module.exports = router