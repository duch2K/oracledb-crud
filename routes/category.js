const Router = require('express')

const controller = require('../controllers/category')

const router = new Router()

router.get('/:id?', controller.getCategories)
router.post('/', controller.createCategory)
router.put('/:id?', controller.updateCategory)
router.delete('/:id?', controller.deleteCategory)

module.exports = router