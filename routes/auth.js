const Router = require('express')
const { check } = require('express-validator')

const authMiddleware = require('../middlewares/auth')
const controller = require('../controllers/auth')

const router = new Router()

router.post(
  '/register',
  [
    check('username', 'Shouldn\'t be empty').notEmpty(),
    check('password', 'Password should contain at least 6 symbols').isLength({ min: 6, max: 255 })
  ],
  controller.registration
)
router.post('/login', controller.login)
router.get('/users', authMiddleware, controller.getUsers)

module.exports = router