const jwt = require('jsonwebtoken')

const generateAccessToken = (user, secret) => {
  const payload = { user }
  return jwt.sign(payload, secret, { expiresIn: '24h' })
}

module.exports = {
  generateAccessToken
}