const oracledb = require('oracledb')

const dbInit = async ({ user, password, connectString, poolAlias }) => {
  try {
    oracledb.outFormat = oracledb.OBJECT

    await oracledb.createPool({
      user, // [username]
      password, // [password]
      connectString, // [hostname]:[port]/[DB service name]
      poolAlias
  
    })

    console.log("Pool created")
    // callback()

  } catch (err) {
    console.log('DB ERROR', err)
    process.exit(1)
  }
}

const closePool = async () => {
  try {
    await oracledb.getPool('my-pool').close(10)
    console.log("Pool closed")

  } catch (err) {
    console.error(err.message)
  }
}

const getConnection = async () => {
  return await oracledb.getConnection('my-pool')
}

module.exports = {
  dbInit, closePool, getConnection
}