const product = require('../db-api/product')

module.exports = {
  async getProducts(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        const { id } = req.params
        result = await product.select({ id })

      } else if (Object.keys(req.query).length > 0) {
        result = await product.select(req.query)

      } else {
        result = await product.select()
      }

      return res.json(result)

    } catch (err) {
      console.log('THROWING GET ERROR', err)
      next(err, req, res)

    } finally {

    }
  },

  async getOrderItems(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await product.selectOrderItems(req.params.id)
      }

      return res.json(result)

    } catch (err) {
      console.log('ERROR', err)
      next(err, req, res)

    } finally {

    }
  },

  async getProductsByCategories(req, res, next) {
    let result = null

    try {
      if (req.query.category) {
        if (Array.isArray(req.query.category)) {
          result = await product.selectByCategories(req.query.category)
        } else {
          result = await product.selectByCategories([req.query.category]) 
        }
      }

      return res.json(result)

    } catch (err) {
      console.log('ERROR', err)
      next(err, req, res)

    } finally {

    }
  },

  async createProduct(req, res, next) {
    let result = null
    
    try {
      result = await product.insert(req.body)
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {

    }
  },

  async createOrderItem(req, res, next) {
    let result = null

    try {
      result = await product.insertOrderItem(req.body)
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {

    }
  },

  async updateProduct(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await product.update(req.body, req.params)
      } else {
        result = await product.update(req.body, req.query)
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING UPDATE ERROR', err)
      next(err, req, res)

    } finally {

    }
  },

  async updateOrderItem(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await product.updateOrderItem({ quantity: req.body.quantity }, {
          product_id: req.params.id,
          customer_id: req.body.customer_id
        })
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING UPDATE ERROR', err)
      next(err, req, res)

    } finally {

    }
  },

  async updateOrderItemIncDec(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await product.updateOrderItemIncDec({ quantity: req.body.quantity }, {
          product_id: req.params.id,
          customer_id: req.body.customer_id
        })
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING UPDATE ERROR', err)
      next(err, req, res)
      
    } finally {

    }
  },

  async deleteProduct(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await product.remove(req.params) 
      } else {
        result = await product.remove(req.query)
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING DELETE ERROR', err)
      next(err, req, res)

    } finally {

    }
  },

  async deleteOrderItem(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await product.removeOrderItem({ ...req.params, ...req.body }) 
      } else {
        result = await product.removeOrderItem({ ...req.query, ...req.body })
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING DELETE ERROR', err)
      next(err, req, res)

    } finally {

    }
  }
}
