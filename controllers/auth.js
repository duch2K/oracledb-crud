const bcrypt = require('bcryptjs')
const { validationResult } = require('express-validator')

const { select, insert } = require('../db-api/customer')
const { generateAccessToken } = require('../helpers')
const { secretKey } = require('../config')

module.exports = {
  async registration(req, res, next) {
    let result = null
    try {
      const { name, surname, username, password } = req.body
      const errors = validationResult(req)

      if (!errors.isEmpty()) {
        return res.json({ message: 'Registration error', errors })
      }

      const candidate = await select({ username })
      console.log('CANDIDATE' ,candidate)
      if (candidate.length > 0) {
        return res.status(400).json({ message: 'User exists' })
      }

      const hashPassword = bcrypt.hashSync(password, 5)
      result = await insert({ name, surname, username, password: hashPassword }) // insert user
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async login(req, res, next) {
    try {
      const { username, password } = req.body
      const result = await select({ username })
      const user = result[0]

      if (!user && user.length < 1) {
        return res.status(400).json({ message: 'User not found' })
      }

      const validPassword = bcrypt.compareSync(password, user.password)
      if (!validPassword) {
        return res.status(400).json({ message: 'Wrong username or password' })
      }

      console.log(user)
      const token = generateAccessToken(user, secretKey)
      return res.json({ token })

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async getUsers(req, res, next) {
    try {
      const result = await select()
      console.log('DECODDED', req.user)
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  }
}
