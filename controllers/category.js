const order = require('../db-api/category')

module.exports = {
  async getCategories(req, res, next) {
    let result = null;

    try {
      if (req.params.id) {
        const { id } = req.params
        result = await order.select({ id })

      } else if (Object.keys(req.query).length > 0) {
        console.log('URL QUERY', req.query)
        result = await order.select(req.query)

      } else {
        result = await order.select()
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING GET ERROR')
      next(err, req, res)

    } finally {
    }
  },

  async createCategory(req, res, next) {
    let result = null
    
    try {
      result = await order.insert(req.body)
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async updateCategory(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await order.update(req.body, req.params)   
      } else {
        result = await order.update(req.body, req.query) 
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING UPDATE ERROR')
      next(err, req, res)
      
    } finally {
    }
  },

  async deleteCategory(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await order.remove(req.params) 
      } else {
        result = await order.remove(req.query)
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING DELETE ERROR')
      next(err, req, res)

    } finally {
    }
  }
}
