const customer = require('../db-api/customer')

module.exports = {
  async getCustomers(req, res, next) {
    let result = null;

    try {
      if (req.params.id) {
        const { id } = req.params
        result = await customer.select({ id })
  
      } else if (Object.keys(req.query).length > 0) {
        result = await customer.select(req.query)
        
      } else {
        result = await customer.select()
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING GET ERROR')
      next(err, req, res)

    } finally {
    }
  },

  async createCustomer(req, res, next) {
    let result = null
    
    try {
      result = await customer.insert(req.body)
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async updateCustomer(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await customer.update(req.body, req.params)   
      } else {
        result = await customer.update(req.body, req.query) 
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING UPDATE ERROR')
      next(err, req, res)
      
    } finally {
    }
  },

  async deleteCustomer(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await customer.remove(req.params) 
      } else {
        result = await customer.remove(req.query)
      }
      return res.json(result)

    } catch (err) {
      console.log('THROWING DELETE ERROR')
      next(err, req, res)

    } finally {
    }
  }
}
