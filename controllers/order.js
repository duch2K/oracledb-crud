const order = require('../db-api/order')

module.exports = {
  async getOrders(req, res, next) {
    let result = null;

    try {
      if (req.params.id) {
        result = await order.select(req.params)

      } else if (Object.keys(req.query).length > 0) {
        result = await order.select(req.query)

      } else {
        result = await order.select()
      }
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async getPaymentMethods(req, res, next) {
    let result = null;

    try {
      if (req.params.id) {
        result = await order.selectPaymentMethods(req.params)

      } else if (Object.keys(req.query).length > 0) {
        result = await order.selectPaymentMethods(req.query)

      } else {
        result = await order.selectPaymentMethods()
      }
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async createOrder(req, res, next) {
    let result = null

    try {
      result = await insert(req.body)  
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async updateOrder(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await order.update(req.body, req.params)   
      } else {
        result = await order.update(req.body, req.query) 
      }
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)
      
    } finally {
    }
  },

  async makeOrder(req, res, next) {
    let result = null

    try {
      const currOrder = await order.select({
        customer_id: req.body.customer_id,
        status_id: 1
      })

      result = await order.update({ 
        status_id: 2,
        total_price: req.body.total_price 
      }, {
        id: currOrder[0].id
      })

      await order.insert({
        customer_id: req.body.customer_id,
        status_id: 1,
        total_price: 0
      })
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  },

  async deleteOrder(req, res, next) {
    let result = null

    try {
      if (req.params.id) {
        result = await order.remove(req.params)
      } else {
        result = await order.remove(req.query)
      }
      return res.json(result)

    } catch (err) {
      console.log(err)
      next(err, req, res)

    } finally {
    }
  }
}
