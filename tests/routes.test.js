const request = require('supertest')

const app = require('../task')
const { closePool } = require('../loaders/db')

beforeAll(() => {
  console.log('Starting tests...')
})

afterAll(async () => {
  await closePool()  
  console.log('Finishing tests...')
})

describe('Customer router:', () => {
  test('should return array with one customer', async () => {
    const response = await request(app).get('/customer/1')
    expect(response.body[0].id).toBe(1);
    // expect(response.body).toEqual(customers);
  })

  test('should create customer and return status 200', async () => {
    const response = await request(app).post('/customer')
      .send({
        name: 'Anthony',
        surname: 'Stark',
        username: 'ironman',
        password: 'qwerty'
      }) 

    expect(response.statusCode).toBe(200)
  })

  test('should update customer and return status 200', async () => {
    const response = await request(app).put('/customer?surname=Stark')
      .send({
        name: 'Tony',
        username: 'irontony'
      })

    expect(response.statusCode).toBe(200)
  })

  test('should delete customer and return status 200', async () => {
    const response = await request(app).delete('/customer?surname=Stark')
    expect(response.statusCode).toBe(200)
  })
})

describe('Order router:', () => {
  test('should return array with one order', async () => {
    const response = await request(app).get('/order?id=1')
    console.log(response.body)
    expect(response.body[0].id).toBe(1);
    // expect(response.body).toEqual(orders);
  })

  test('should create order and return result', async () => {
    const response = await request(app).post('/order')
      .send({
        customer_id: 1,
        total_price: 111,
        status_id: 1,
        payment_method_id: 1
      })
console.log(response)
    expect(response.statusCode).toBe(200)
  })

  test('should update order and return status 200', async () => {
    const response = await request(app).put('/order?total_price=111')
      .send({
        status_id: 2
      })

    expect(response.statusCode).toBe(200)
  })

  test('should delete order and return result', async () => {
    const response = await request(app).delete('/order?total_price=111')
    expect(response.statusCode).toBe(200)
  })
})
