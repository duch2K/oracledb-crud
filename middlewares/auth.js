const jwt = require('jsonwebtoken')

const { secretKey } = require('../config')

module.exports = function(req, res, next) {
  if (req.method === 'OPTIONS') {
    next()
  }

  try {
    const token = req.headers.authorization.split(' ')[1]
    if (!token) {
      return res.status(403).json({ message: 'Unauthorized user' })
    }

    const decoddedData = jwt.verify(token, secretKey)
    req.user = decoddedData
    next()

  } catch (err) {
    console.log(err)
    return res.status(403).json({ message: 'Unauthorized user' })
  }
}