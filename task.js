const express = require('express')
const cors = require('cors')

const router = require('./routes')
const { dbInit, closePool } = require('./loaders/db')
const {
  dbUser: user,
  dbPassword: password,
  dbConnectString: connectString,
  dbPoolAlias: poolAlias,
  appPort 
} = require('./config')

const app = express()

const exitApp = async () => {
  await closePool()
  process.exit(1)
}

const appInit = async () => {
  app.use(cors())
  app.use(express.json())
  app.use('/', router)
  app.use((error, req, res, next) => {
    res.status(error.status)

    res.json({
      status: error.status,
      message: error.message,
      stack: error.stack
    })
  })

  app.listen(appPort, () => console.log(`Server has started on port ${appPort}`))
}

const startUp = async () => {
  try {
    await dbInit({ user, password, connectString, poolAlias })

    await appInit()

  } catch (err) {
    console.log(err)
    exitApp()

  } finally {

  }
}

startUp()
process.on('SIGTERM', exitApp)
process.on('SIGINT', exitApp)

module.exports = app
