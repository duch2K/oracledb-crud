const { getConnection } = require('../loaders/db')
const order = require('./order')

const table = 'PRODUCT'
const categoryRelTable = 'PRODUCT_CATEGORY_REL'
const columns = [
  'id',
  'name',
  'price',
  'description',
  'image',
  'manufacturer_id',
  'on_market',
  'max_store',
  'created_user',
  'updated_user',
  'created_date',
  'updated_date'
]

// SELECT
const select = async (where = {}) => {
  let sql = 'select'
  let conn

  try {
    let lowColumns = []

    for (const elem of columns) {
      lowColumns.push(`${elem} as "${elem.toLowerCase()}"`)
    }

    sql = `${sql} ${lowColumns.join(',')} from "${table}"`

    if (Object.keys(where).length > 0) {
      let fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      fields = fields.join(' and ')

      sql = `${sql} where ${fields}`
    }

    console.log('QUERYY: ', sql)
    conn = await getConnection()
    const { rows: result } = await conn.execute(sql, where)
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const selectByCategories = async (categoryIds = []) => {
  let sql = 'select'
  let conn

  try {
    let lowColumns = []
    for (const elem of columns) {
      lowColumns.push(`p.${elem} as "${elem.toLowerCase()}"`)
    }

    sql = `
      ${sql} ${lowColumns.join(',')} from "${table}" p
        join "${categoryRelTable}" pc on p.id = pc.product_id
        join "CATEGORY" c on c.id = pc.category_id
    `

    if (categoryIds.length > 0) {
      let fields = []
      for (const elem of categoryIds) {
        fields.push(`category_id = :category_id`)
      }
      fields = fields.join(' or ')

      sql = `${sql} where ${fields}`
    }

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const { rows: result } = await conn.execute(sql, categoryIds)
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const selectOrderItems = async (customerId, productId = 0) => {
  let sql = 'select'
  let conn

  try {
    const userOrder = await order.select({
      customer_id: customerId,
      status_id: 1
    })

    if (userOrder.length < 1) {
      await order.insert({
        customer_id: customerId,
        status_id: 1,
        total_price: 0
      })
    }

    let lowColumns = []
    for (const elem of columns) {
      lowColumns.push(`p.${elem} as "${elem.toLowerCase()}"`)
    }
    sql = `
      ${sql} ${lowColumns.join(',')}, oi.quantity as "quantity" from "${table}" p
        join "ORDER_ITEM" oi on oi.product_id = p.id
        join "ORDER" o on o.id = oi.order_id
        join "CUSTOMER" c on c.id = o.customer_id
        where o.status_id = :status_id and c.id = :id
        ${productId ? 'and p.id = :product_id' : ''}
    `

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const binds = productId ? [ 1, customerId, productId ] : [ 1, customerId ]
    const { rows: result } = await conn.execute(sql, binds)
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

// INSERT
const insert = async (order) => {
  let sql = `insert into "${table}"`

  let fields = []

  let conn, result = null

  try {
    for (const key in order) {
      fields.push(key)
    }

    sql = `
      ${sql} (${fields.join(',')}, created_date, updated_date)
      values (${fields.map(el => `:${el}`).join(',')}, sysdate, sysdate)
    `

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const result = await conn.execute(sql, order)
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const insertOrderItem = async (orderItem) => {
  const { product_id, customer_id, quantity } = orderItem

  let sql = `insert into "ORDER_ITEM"`
  let conn

  try {
    const item = await selectOrderItems(customer_id, product_id)
    console.log("ITEm", item)
    if (item.length < 1) {
      let orderId = await order.select({
        customer_id,
        status_id: 1
      })
      orderId = orderId[0].id
  
      sql = `
        ${sql} (product_id, order_id, quantity, created_date, updated_date)
          values (:product_id, :order_id, :quantity, sysdate, sysdate)
      `
  
      console.log('QUERY: ', sql)
      conn = await getConnection()
      const result = await conn.execute(sql, {
        order_id: orderId,
        product_id,
        quantity,
      })
      await conn.commit()
      return result

    } else {
      throw new Error('Item exists!')
    }

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

// UPDATE
const update = async (updates, where = {}) => {
  let sql = `update "${table}" set`

  let fields = []
  let conn

  try {
    for (const key in updates) {
      fields.push(`${key} = :${key}`)
    }
    sql = `${sql} ${fields.join(',')}`

    if (Object.keys(where).length > 0) {
      fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      sql = `${sql} where ${fields.join(' and ')}`
    }

    console.log('QUERY: ', sql)
    const binds = [ ...Object.values(updates), ...Object.values(where) ]
    conn = await getConnection()
    const result = await conn.execute(sql, binds)
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const updateOrderItem = async (updates, where = {}) => {
  let orderId = await order.select({
    customer_id: where.customer_id,
    status_id: 1
  })
  orderId = orderId[0].id
  console.log(orderId)

  let sql = `update "ORDER_ITEM" set`

  let conn

  try {
    sql = `${sql} quantity = :quantity where product_id = :product_id and order_id = :order_id`

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const result = await conn.execute(sql, {
      quantity: updates.quantity,
      order_id: orderId,
      product_id: where.product_id
    })
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const updateOrderItemIncDec = async (updates, where) => {
  let orderId = await order.select({
    customer_id: where.customer_id,
    status_id: 1
  })

  orderId = orderId[0].id
  console.log(orderId)

  let sql = `update "ORDER_ITEM" set`

  let conn, result = null

  try {
    sql = `${sql} quantity = quantity + :quantity where product_id = :product_id and order_id = :order_id`

    console.log('QUERY: ', sql)
    conn = await getConnection()
    result = await conn.execute(sql, {
      quantity: updates.quantity,
      order_id: orderId,
      product_id: where.product_id
    })
    await conn.commit()

  } catch (err) {
    console.log(err)
    

  } finally {
    if (conn) await conn.close()
    return result
  }
}

// DELETE
const remove = async (where = {}) => {
  let sql = `delete from "${table}"`

  let conn

  try {
    if (Object.keys(where).length > 0) {
      let fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      sql = `${sql} where ${fields.join(' and ')}`
    }

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const result = await conn.execute(sql, where)
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const removeOrderItem = async (where = {}) => {
  console.log('HEYYY', where)
  let orderId = await order.select({
    customer_id: where.customer_id,
    status_id : 1
  })
  orderId = orderId[0].id

  let sql = `
    delete from "ORDER_ITEM"
      where product_id = :product_id and
      order_id = :order_id
  `
  let conn

  try {

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const result = await conn.execute(sql, {
      order_id: orderId,
      product_id: where.id
    })
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

module.exports = {
  select,
  insert,
  update,
  remove,
  selectByCategories,
  selectOrderItems,
  insertOrderItem,
  updateOrderItem,
  updateOrderItemIncDec,
  removeOrderItem
}