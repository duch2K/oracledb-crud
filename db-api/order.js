const { getConnection } = require('../loaders/db')

const table = 'ORDER'
const orderColumns = [
  'id',
  'customer_id',
  'total_price',
  'status_id',
  'payment_method_id',
  'created_user',
  'updated_user',
  'created_date',
  'updated_date'
]
const paymentColumns = [
  'id',
  'name',
  'is_active',
  'created_user',
  'updated_user',
  'created_date',
  'updated_date'
]

// SELECT
const select = async (where = {}) => {
  let sql = 'select'
  let conn

  try {
    let lowColumns = []

    for (const elem of orderColumns) {
      lowColumns.push(`${elem} as "${elem.toLowerCase()}"`)
    }

    sql = `${sql} ${lowColumns.join(',')} from "${table}"`

    if (Object.keys(where).length > 0) {
      let fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      fields = fields.join(' and ')

      sql = `${sql} where ${fields}`
    }

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const { rows: result } = await conn.execute(sql, where)
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

const selectPaymentMethods = async (where = {}) => {
  let sql = 'select'
  let conn

  try {
    let lowColumns = []

    for (const elem of paymentColumns) {
      lowColumns.push(`${elem} as "${elem.toLowerCase()}"`)
    }

    sql = `${sql} ${lowColumns.join(',')} from "PAYMENT_METHOD"`

    if (Object.keys(where).length > 0) {
      let fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      fields = fields.join(' and ')

      sql = `${sql} where ${fields}`
    }

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const { rows: result } = await conn.execute(sql, where)
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

// INSERT
const insert = async (order) => {
  let sql = `insert into "${table}"`

  let fields = []

  let conn

  try {
    for (const key in order) {
      fields.push(key)
    }

    sql = `
      ${sql} (${fields.join(',')}, created_date, updated_date)
      values (${fields.map(el => `:${el}`).join(',')}, sysdate, sysdate)
    `

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const result = await conn.execute(sql, order)
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

// UPDATE
const update = async (updates, where = {}) => {
  let sql = `update "${table}" set`

  let fields = []

  let conn

  try {
    for (const key in updates) {
      fields.push(`${key} = :${key}`)
    }
    sql = `${sql} ${fields.join(',')}`

    if (Object.keys(where).length > 0) {
      fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      sql = `${sql} where ${fields.join(' and ')}`
    }

    console.log('QUERY: ', sql)
    const binds = [ ...Object.values(updates), ...Object.values(where) ]
    conn = await getConnection()
    const result = await conn.execute(sql, binds)
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

// DELETE
const remove = async (where = {}) => {
  let sql = `delete from "${table}"`

  let conn

  try {
    if (Object.keys(where).length > 0) {
      let fields = []
      for (const key in where) {
        fields.push(`${key} = :${key}`)
      }
      sql = `${sql} where ${fields.join(' and ')}`
    }

    console.log('QUERY: ', sql)
    conn = await getConnection()
    const result = await conn.execute(sql, where)
    await conn.commit()
    return result

  } catch (err) {
    console.log(err)
    return err

  } finally {
    if (conn) await conn.close()
  }
}

module.exports = {
  select,
  insert,
  update,
  remove,
  selectPaymentMethods
}